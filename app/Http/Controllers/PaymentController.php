<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use GuzzleHttp\Client;
use Illuminate\Support\Str;

class PaymentController extends Controller
{
    public function payment(Request $request)
    {
        $order = Order::create([
            'amount' => $request->amount,
            'method' => $request->method,
            'order_id' => 'invoice-' . Str::random(5)
        ]);

        // dd($order);

        $response_midtrans = $this->midtrans_store($order);

        return response()->json([
            'response code' => '200',
            'response message' => 'Success',
            'data' => $response_midtrans
        ]);
    }

    protected function midtrans_store($order)
    {
        // dd($order);
        $server_key = base64_encode(config('app.midtrans.server_key'));
        $base_uri = config('app.midtrans.base_uri');
        // dd($base_uri);
        $client = new Client([
            'base_uri' => $base_uri
        ]);
        // dd($client);
        $headers = [
            'Accept'            =>  'application/json',
            'Authorization'     =>  'Basic ' . $server_key,
            'Content-Type'      =>  'application/json'
        ];

        switch ($order->method) {
            case 'bca':
                $body = [
                    'payment_type' => 'bank_transfer',
                    'transaction_details' => [
                        'order_id'  =>  $order->order_id,
                        'gross_amount'  =>  $order->amount
                    ],
                    'bank_transfer' =>  [
                        'bank'  =>  'bca'
                    ]
                ];
            break;

            case 'permata':
                $body = [
                    'payment-type' => 'permata',
                    'transaction_details' => [
                        'order_id'  =>  $order->order_id,
                        'gross_amount'  =>  $order->amount
                    ]
                ];
            break;
        default:
            $body = [];
        break;

        }

        $res = $client->post('/v2/charge', [
            'headers'   =>  $headers,
            'body'      =>  json_encode($body)
        ]);
        // dd(json_decode($res->getBody()));

        return json_decode($res->getBody());
    }
}
