<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Auth')->group(function() {
    Route::get('register', 'RegisterController@index');
    Route::post('post-register', 'RegisterController@Register');
    Route::get('login', 'LoginController@index');
    Route::post('post-login', 'LoginController@Login');
    Route::post('logout', 'LogoutController');
});

Route::get('home', 'HomeController@index');

Route::post('payment-midtrans', 'PaymentController@payment');
